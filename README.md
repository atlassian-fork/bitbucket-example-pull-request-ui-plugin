This plugin adds a TODO list into the pull request UI.

It is reference material for the [Pull Request Overview tutorial](https://developer.atlassian.com/stash/docs/latest/tutorials-and-examples/pull-request-overview.html).

It adds UI for displaying and adding TODOs:

![Pull request overview links](https://bytebucket.org/atlassian/bitbucket-example-pull-request-ui-plugin/raw/7de556dec3ddec103b2abbb04aa2084166b59834/images/add_TODO.png)

And a dialog that lists the TODOs:

![TODOs dialog](https://bytebucket.org/atlassian/bitbucket-example-pull-request-ui-plugin/raw/7de556dec3ddec103b2abbb04aa2084166b59834/images/view_TODOs.png)
